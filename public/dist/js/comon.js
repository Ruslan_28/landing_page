$(document).ready(function(){ 

    //custom select

    $('select').each(function(){
        var $this = $(this), numberOfOptions = $(this).children('option').length;
    
        $this.addClass('select-hidden'); 
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).text());
    
        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);
    
        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }
    
        var $listItems = $list.children('li');
    
        $styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
            
        });
        
        $listItems.click(function(e) {

            
            

            
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            
            
        });
    
        $(document).click(function() {
            $styledSelect.removeClass('active');
            $list.hide();
        });


       $(".select-styled").on('click', function() {
           
            if( $(".select-hidden").val() == "En" ) {
                $(".select-options").find(">:first-child").css( "display", "none" );
                $(".select-options").find(">:last-child").css( "display", "block" );
            }
            
            if( $(".select-hidden").val() == "Ru" ) {
                $(".select-options").find(">:first-child").css( "display", "block" );
                $(".select-options").find(">:last-child").css( "display", "none" );
            }
       })

    });

    //scroll header animate

    const header = $("header");
    var lastScrollTop = 0;
    $(window).scroll(function(event){
    var st = $(this).scrollTop();
    if (st > lastScrollTop){
        if(!header.hasClass('transformHeader')){
            header.addClass('transformHeader');
        }
    } else {
        header.removeClass('transformHeader');
    }
    lastScrollTop = st;
    });

    //scroll page 

    $(".headerBox, .navFooter").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
 

    //animate

    var mql = window.matchMedia('all and (max-width: 1300px)');
    if (mql.matches) {
       return
    } else {
        $(".imgPage, .titleVideo, .descriptionVideo, .videoBtn, .boxBlock, .itemBox").animated("fadeInUp", "fadeOutDown");
        $(".advantageContainer, .theCostContainer").animated("fadeInLeft", "fadeOutRight");
    
        $(".animateRgt").animated("fadeInRight", "fadeOutRight");
        $(".animateLft").animated("fadeInLeft", "fadeOutLeft");
    }

})

